# An example of extension of the jupyter stack 'scipy-notebook'
# with pip modules ('pip install ...') and their system dependancies ('apt-get install -y ...')
ARG JUPYTER_IMG=${JUPYTER_IMG:-quay.io/jupyter/scipy-notebook}
FROM ${JUPYTER_IMG}

ARG BUILD_DATE=latest

# Copy app config from the build context
COPY . /ifb/apprepo/jupyter-biospherapp-$BUILD_DATE
WORKDIR /ifb/apprepo/jupyter-biospherapp-$BUILD_DATE

USER root

# # Add conda channels and install Mamba
# RUN conda config --add channels R --system \
#   && conda config --add channels bioconda --system \
#   && conda config --add channels conda-forge --system \
#   && conda update -y -n base -c conda-forge conda \
#   && conda install -y -n base -c conda-forge mamba

# Install system dependencies packages with apt-get
RUN if [ -f "apt.txt" ]; then \
  apt-get update -qq; \
  apt-get -y --no-install-recommends install `grep -v "^#" apt.txt | tr '\n' ' '`; \
  fi

# Use a conda environment
RUN if [ -f "environment.yml" ]; then \
  mamba env create -f environment.yml \
  && conda_env=$( grep name environment.yml | awk '{ print $2}' ) \
  && mamba install -y -n ${conda_env} nb_conda \
  && "${CONDA_DIR}/envs/${conda_env}/bin/python" -m ipykernel install --user --name="${conda_env}" \
  && fix-permissions "${CONDA_DIR}" \
  && fix-permissions "/home/${NB_USER}"; \
  fi

# Install JupyterLab extensions
RUN pip install jupyterlab-rise jupyterlab-fasta jupyterlab_execute_time ipympl
RUN mamba install -c bioconda hmmer pyrodigal  mafft fasttree iqtree seqkit pandas mmseqs2
RUN pip install mdmparis_defense_finder

USER ${NB_USER}
WORKDIR /home/${NB_USER}

# Install training data
RUN git clone https://gitlab.pasteur.fr/mdm-lab/classes/defensefinder_qlife_2024.git
