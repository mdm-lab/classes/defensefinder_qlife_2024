# DefenseFinder Practical Session for Qlife summer school


## Genomic detection of anti-phage defense systems using Hidden Markov Models 

In natural environments, bacteriophages outnumber bacteria and represent an important threat for these organisms. 
The arms race between bacteria and bacteriophages drives the diversification of bacterial anti-phage defense systems. 
While it was long believed that the anti-phage arsenal of bacteria was reduced to a few systems, the recent discoveries of more than a hundred previously unknown anti-phage systems has revolutionized the field. 
These discoveries was essentially made possible by the analysis of large amounts of bacterial genomes which itself relies on publicly available datasets and bioinformatics analyses [1, 2, 3].


This practical course aims at introducing the principles and methods used in genomics, focusing on the study of anti-phage defense systems. Through the discovery and characterization of a novel anti-phage systems, the students will be introduced to

1. Fundamental principles of anti-phage systems (what is a defense system and defense islands, what are the signatures of defense systems in bacterial genomes and how to leverage them to discover new anti-phage systems) ;
2. Fundamental principles of biological sequence analysis (notion of homology, multiple sequence alignment, etc.) ;
3. In practice, how to mine a database of bacterial genomes to identify anti-phage systems using the DefenseFinder tool [4] ;  
4. How to search for homologs of a protein of interest in a database of genomes and a infer the evolutionary history of a protein (Construction of multiple sequence alignments of a set of proteins, building profile HMM to perform homology search and building phylogenetic trees).


References 
- [1]  Doron, S., Melamed, S., Ofir, G., Leavitt, A., Lopatina, A., Keren, M., ... & Sorek, R. (2018). Systematic discovery of antiphage defense systems in the microbial pangenome. Science, 359(6379), eaar4120.
- [2]  Gao, L., Altae-Tran, H., Böhning, F., Makarova, K. S., Segel, M., Schmid-Burgk, J. L., ... & Zhang, F. (2020). Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science, 369(6507), 1077-1084.
- [3]  Millman, A., Melamed, S., Leavitt, A., Doron, S., Bernheim, A., Hör, J., ... & Sorek, R. (2022). An expanded arsenal of immune systems that protect bacteria from phages. Cell host & microbe, 30(11), 1556-1569.
- [4]  Tesson, F., Hervé, A., Mordret, E., Touchon, M., D’humières, C., Cury, J., & Bernheim, A. (2022). Systematic and quantitative view of the antiviral arsenal of prokaryotes. Nature communications, 13(1), 2561.