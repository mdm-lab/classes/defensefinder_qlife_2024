import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

from Bio import Phylo
from matplotlib.font_manager import FontProperties
import seaborn as sns

def has_ds_nearby(gene_acc, defense_finder_genes, neighbourhood_size=15):
    """
    Takes as input a gene accession and checks whether a defense system has been identified in the genomic neighbourhood of the gene.
    gene_acc (str):
    defense_finder_genes (pd.DataFrame):
    neighbourhood_size (int):
    Return (bool): True if a defense system is found nearby, else False
    """
    # Generate list of neighbours
    replicon, gene_id = gene_acc.split("_")[0], int(gene_acc.split("_")[1])
    neighbours = ["_".join([replicon, "{:05}".format(gene_id + stride)]) for stride in range(-neighbourhood_size, neighbourhood_size) if stride != 0]

    # Identify if neighbours are found in the list of defense genes
    if gene_acc in defense_finder_genes["hit_id"].values:  # /!\ If gene_acc is found in a defense system, do not take this system into account
        sys_id = defense_finder_genes.loc[defense_finder_genes["hit_id"] == gene_acc, "sys_id"].values[0]
        ds_nearby = defense_finder_genes.loc[(defense_finder_genes["hit_id"].isin(neighbours)) & (defense_finder_genes["sys_id"] != sys_id)]
    else:
        ds_nearby = defense_finder_genes.loc[(defense_finder_genes["hit_id"].isin(neighbours))]

    return (ds_nearby.shape[0] > 0)


if __name__ == '__main__':
    df_genes = pd.read_csv("C:/Users/hvays/These/quotidien/courses/Qlife/defensefinder_qlife_2024/results/1_defense_finder/All_genomes_defense_finder_genes.tsv", sep="\t")
    print(has_ds_nearby("KLPN001.0722.00567.C001_03556", df_genes, 15))

def plot_tree(treefile, leaves_to_color=[], ax=None):
    # load the tree from the file
    with open(treefile) as f:
        tree = Phylo.read(f, format="newick")
    tree.root_at_midpoint()
    tree.ladderize()
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(12, 15))
    Phylo.draw(tree, axes=ax, do_show=False, show_confidence=False)
    
    xmax = np.max([t.get_position()[0] for t in ax.texts])
    biggest_leaves = np.max([len(t.get_text()) for t in ax.texts])
    leaves = []
    for t in ax.texts:
        X = t.get_position()[0]
        Y = t.get_position()[1]
        if t.get_text().split("_")[0].strip() in leaves_to_color:
            t.set_bbox(dict(x=xmax,
                    y=Y-.5, # Y-axe is reversed 
                    facecolor="C1",
                    edgecolor="C1",
                    alpha=1,
                    zorder=100))
        ax.hlines(Y, X, xmax, "lightgrey", linestyle="dotted", lw=0.1)
        t.set_x(xmax)
        t.set_text("{:>{width}}".format(t.get_text().strip(), width=biggest_leaves))
        t.set_fontproperties("monospace")
        t.set_fontsize(8)
        #t.set_ha("right")
        #t.set_va("center")
        t.set_zorder(-1)
        leaves.append(t.get_text().strip()) # get leaves as they are when reading the tree from top to bottom
    ax.set_xticklabels("",visible=False)
    ax.set_yticklabels("",visible=False)
    ax.set_xlabel("", visible=False)
    ax.set_ylabel("", visible=False)
    ax.set_xlim(0, xmax+biggest_leaves/45.)
    sns.despine(left=True, bottom=True)
    ax.tick_params(length=0)
